
const TestAPI = {
  handler: async (req, reply) => {
    if (req.payload && req.payload.firstName && req.payload.lastName) {
      return reply.response(`Hi ${req.payload.firstName} ${req.payload.lastName} , This is my first API`);
    }else {
      return reply.response('Testing my first API');
    }
  }
};

const Test = {
  handler : (req, h) => {
    console.log('testing GET APIS');
    return h.response({
      message : 'Hello world',
      data : [1,2,3,4,5]
    });
  }
};

exports.routes = [{
  method: 'POST',
  path: '/test',
  config: TestAPI
},{
  method: 'GET',
  path: '/test',
  config: Test
}];
