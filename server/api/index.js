const Test = require('./handlers/test');

exports.plugin = {
  name: 'api',
  register: (server) => {
    server.route(Array.prototype.concat(Test.routes));
  }
};
