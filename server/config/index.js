const Development = require('./dev');
const config = {
  development: Development
};

const env = process.env.NODE_ENV || 'development';
config.env = env;

module.exports = config[env];
