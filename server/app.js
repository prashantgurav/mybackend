const config = require('./config');
const manifest = config.manifest;
const Glue = require('@hapi/glue');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const startServer = async function () {
  try {
    // Setup server
    if (cluster.isMaster) {
      // Fork workers.
      for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
      }
      cluster.on('online', (worker) => {
        console.log('worker is online : ', worker.id);
      });
      cluster.on('exit', (worker) => {
        console.log(`worker ${worker.process.pid} died`);
        cluster.fork();
      });

    } else {
      const server = await Glue.compose(manifest, {
        relativeTo: __dirname
      });
      await server.start();
      console.log('Server is listening on ' + server.info.uri.toLowerCase());
    }
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

startServer();
